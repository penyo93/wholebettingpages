function SimplifyTennisNames(teamName) {
    let firstPart = teamName.split(" ")[0];
    let secondPart = teamName.split(" ")[1];

    if (
        secondPart.includes("1") ||
        secondPart.includes("2") ||
        secondPart.includes("3") ||
        secondPart.includes("4") ||
        secondPart.includes("5") ||
        secondPart.includes("wins")
    ) {
        secondPart = "";
    }

    if (firstPart.includes(".")) {
        firstPart = "";
    }

    if (firstPart === "" && secondPart === "") {
        console.log("Snow match name parse error: " + teamName);
        return teamName;
    }

    if (secondPart !== "" && firstPart.length > 2) {
        firstPart = "";
    }

    let retValue = (firstPart + " " + secondPart).trim();
    if (retValue === "" || retValue === undefined) {
        console.log("Error occured: " + teamName + " - " + retValue);
        return "error";
    }

    return retValue;
}


function SimplifySoccerName(teamName) {
    let retValue = teamName.split("-")[0].trim();
    return retValue;
}

function SimplifyBasketballName(teamName) {
    let retValue = teamName.split(" ")[0];
    return retValue;
}


function SimplifyBaseballName(teamName) {
    let retValue = teamName.split("-")[0].trim();
    return retValue;
}

function SimplifyNFLName(teamName) {
    let retValue = teamName.split("-")[0].trim();
    return retValue;
}


export {SimplifyTennisNames, SimplifySoccerName,SimplifyBasketballName, SimplifyBaseballName, SimplifyNFLName}