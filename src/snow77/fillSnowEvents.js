function fillSnowEvents(inputJson, snowEvent, switchedMatch, markets, lines, teamSide) {
    const handicapHomeMarketsCount =
        inputJson["sc"]["schl"][markets]["g"][lines]["ts"][teamSide]["ls"]["s"].length;

    if (handicapHomeMarketsCount !== undefined && handicapHomeMarketsCount > 0) {

        let side = (teamSide === 0 && switchedMatch !== 1) || (teamSide === 1 && switchedMatch !== 0) ? "home" : "away";

        const handicapHomeMarketID =
            inputJson["sc"]["schl"][markets]["g"][lines]["ts"][teamSide]["ls"]["s"][0]["i"];
        let handicapHomeMarketName =
            inputJson["sc"]["schl"][markets]["g"][lines]["ts"][teamSide]["ls"]["s"][0]["p"];
        const americanOdds =
            inputJson["sc"]["schl"][markets]["g"][lines]["ts"][teamSide]["ls"]["s"][0]["o"];
        const handicapHomeMarketOdds = ConvertAmericanOddstoDecimal(americanOdds);

        //console.log("Handicap: " + handicapHomeMarketName + " odds: " + handicapHomeMarketOdds + " count: " + handicapHomeMarketsCount + " (" + handicapHomeMarketID + ")");

        let prefix = "_";
        if (Number(handicapHomeMarketName) === 0) {
            handicapHomeMarketName = "";
            prefix = "";
        }
        snowEvent["odds"][side + prefix + handicapHomeMarketName] = handicapHomeMarketOdds;
        snowEvent["odds"][side + prefix + handicapHomeMarketName + "Id"] = handicapHomeMarketID;
        snowEvent["odds"][side + prefix + handicapHomeMarketName + "American"] = americanOdds;
    }

    const matchWinnerHomeMarketsCount =
        inputJson["sc"]["schl"][markets]["g"][lines]["ts"][teamSide]["ls"]["m"].length;
    if (matchWinnerHomeMarketsCount !== undefined && matchWinnerHomeMarketsCount > 0) {

        let side = (teamSide === 0 && switchedMatch !== 1) || (teamSide === 1 && switchedMatch !== 0) ? "home" : "away";

        const matchWinnerHomeMarketID =
            inputJson["sc"]["schl"][markets]["g"][lines]["ts"][teamSide]["ls"]["m"][0]["i"];
       // const matchWinnerHomepMarketName =
        //    inputJson["sc"]["schl"][markets]["g"][lines]["ts"][teamSide]["ls"]["m"][0]["p"];
        const americanOdds =
            inputJson["sc"]["schl"][markets]["g"][lines]["ts"][teamSide]["ls"]["m"][0]["o"];
        //console.log("MatchWinner: " + matchWinnerHomepMarketName + " odds: " + matchWinnerHomeMarketOdds + " (" + matchWinnerHomeMarketID + ")");

        snowEvent["odds"][side] = ConvertAmericanOddstoDecimal(americanOdds);
        snowEvent["odds"][side + "Id"] = matchWinnerHomeMarketID;
        snowEvent["odds"][side + "American"] = americanOdds;
    }

    const totalHomeMarketsCount =
        inputJson["sc"]["schl"][markets]["g"][lines]["ts"][teamSide]["ls"]["t"].length;
    if (totalHomeMarketsCount !== undefined && totalHomeMarketsCount > 0) {
        const totalHomeMarketID =
            inputJson["sc"]["schl"][markets]["g"][lines]["ts"][teamSide]["ls"]["t"][0]["i"];
        const totalHomeMarketName =
            inputJson["sc"]["schl"][markets]["g"][lines]["ts"][teamSide]["ls"]["t"][0]["p"];
        const americanOdds =
            inputJson["sc"]["schl"][markets]["g"][lines]["ts"][teamSide]["ls"]["t"][0]["o"];
        //console.log("Total: 0 " + totalHomeMarketName + " odds: " + totalHomeMarketOdds + " count: " + totalHomeMarketsCount + "(" + totalHomeMarketID + ")");

        let direction = teamSide === 0 ? "over_" : "under_";
        snowEvent["odds"][direction + totalHomeMarketName] = ConvertAmericanOddstoDecimal(americanOdds);
        snowEvent["odds"][direction + totalHomeMarketName + "Id"] = totalHomeMarketID;
        snowEvent["odds"][direction + totalHomeMarketName + "American"] = americanOdds;
    }
}

function ConvertAmericanOddstoDecimal(americanOdds) {
    // positive
    // Decimal odds = (US odds/100) +1

    // negative
    // decimal odds = 1 - (100 / - American odds)
    if (Number(americanOdds) === 0) {
        console.log("American odds is 0");
        return 0;
    }

    let decimalOdds;
    if (Number(americanOdds) > 0) {
        decimalOdds = 1 + Number(americanOdds) / 100;
        return decimalOdds;
    } else {
        // 1 - (100 / - American odds)
        decimalOdds = 1 - 100 / Number(americanOdds);
        return decimalOdds;
    }
}

export {fillSnowEvents}