import puppeteer from "puppeteer";
import readline from "readline";
import {fillSnowEvents} from "./fillSnowEvents.js";
import {
    SimplifyTennisNames,
    SimplifySoccerName,
    SimplifyBasketballName,
    SimplifyBaseballName,
    SimplifyNFLName
} from "./simplifyNames.js";

let _browser = null;
let _page = null;
let _userAgent = null;

let _cookiesAsString = null;

let _snowEventsArray = [];

async function StartBrowser() {
    try {
        _browser = await puppeteer.launch({
            headless: false,
        });

        _userAgent = await _browser.userAgent();

        //let page = await browser.newPage();

        _page = (await _browser.pages())[0];
        await _page.goto("https://www.snow77.com");
        await sleep(100);

        const cookies = await _page.cookies();
        _cookiesAsString = "";
        cookies.forEach((cookie) => {
            _cookiesAsString += `${cookie.name}=${cookie.value}; `;
        });
        console.log("cookies: " + _cookiesAsString);

        _page.on("response", async (response) => {
            if (response.url().endsWith("0")) {
                let resp = await response.text();
                //console.log("response code: "+ response.status());
               // console.log("response: "+ resp);
                matchArrayFormatter(resp);
            }
        });

        setTimeout(GetEventsUpdate, 2000);
    } catch (e) {
        console.log("StartOctoBrowser exception caught: " + e.message + " " + e.stack);
    }
}

StartBrowser();

async function GetEventsUpdate() {
    const refreshButton = "body > nolink > nolink > nolink > nolink > nolink > app-root > app-main-layout > div.container-fluid > app-schedule > div.main-home.pb-4 > div > div > div:nth-child(1) > div.text-center.d-flex.align-items-end.header-sports > button.btn-update-desktop";

    if (_page !== null) {
        try {
            const next = await _page.waitForSelector(refreshButton, { timeout: 15000 });
            if (next != null) {
                //console.log("Click!")
                await _page.click(refreshButton);
                setTimeout(GetEventsUpdate, 2000);
            }
        } catch (e) {
            console.log("Click exception occured. (Snow77)",e.stack);
            setTimeout(GetEventsUpdate, 2000);
            //   logger.error(e);s
            //   await browser.close();
            //   return false;
        }
    } else {
        console.log("Button is null! (Snow77)");
        setTimeout(GetEventsUpdate, 2000);
    }
}

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

rl.on("SIGINT", async function () {
    console.log("Stopping...");
    await _browser.close();

    process.exit(1);
});

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

function matchArrayFormatter(input) {
    //console.log(JSON.stringify(_snowEventsArray, null, 2));
    console.log("Snow events count: " + _snowEventsArray.length);
    _snowEventsArray = [];

    const inputJson = JSON.parse(input);
    //const inputJson = input
    // console.log(JSON.stringify(inputJson, null, 2));
    // return

    if (inputJson.length === undefined) {
        matchFormatter(inputJson);
        return;
    }

    //console.log("Length: " + inputJson.length);

    for (let match in inputJson) {
        matchFormatter(inputJson[match]);
    }
}

function matchFormatter(inputJson) {
    //console.log(JSON.stringify(inputJson));
    //console.log(inputJson);

   // const leagueName = inputJson["sc"]["l"];
    //console.log("leagueName: " + leagueName);

    for (let markets in inputJson["sc"]["schl"]) {
        const snowEvent = {};
        snowEvent.odds = {};

        const league2 = inputJson["sc"]["schl"][markets]["l"];
        if (league2.includes("LIVE") || league2.includes("Props")) {
            //console.log("Skip this line for now! Name: " + name + " league2: " + league2);
            continue;
        }

        let matchName = "";
        let matchNameEdited = "";

        for (let lines in inputJson["sc"]["schl"][markets]["g"]) {
            const name = inputJson["sc"]["schl"][markets]["g"][lines]["n"];
            //console.log("Name: " + name + " league2: " + league2);

            if (name.includes("Wagering")) {
                //console.log("Skip this line for now! Name: " + name + " league2: " + league2);
                continue;
            }

            if (matchName === "") {
                matchName = name;
                matchNameEdited = matchName.split(" - ")[0];
                matchNameEdited = matchNameEdited.replace(" vs ", " v ");
                snowEvent.name = matchNameEdited;
            }

            let sport = inputJson["sc"]["s"].toString();
            if (sport.includes("Other")){
                sport = inputJson["sc"]["l"].toString().split("-")[0].trim()
            }

            let homeName = inputJson["sc"]["schl"][markets]["g"][lines]["ts"][0]["n"];
            switch (sport) {
                case "Soccer": homeName = SimplifySoccerName(homeName); break;
                case "Tennis": homeName = SimplifyTennisNames(homeName); break;
                case "Basketball": homeName = SimplifyBasketballName(homeName); break;
                case "Baseball": homeName = SimplifyBaseballName(homeName); break;
                case "Football": homeName = SimplifyNFLName(homeName); break;
            }
            //console.log("homeName: " + homeName);
            let awayName = inputJson["sc"]["schl"][markets]["g"][lines]["ts"][1]["n"];
            switch (sport) {
                case "Soccer": awayName = SimplifySoccerName(awayName); break;
                case "Tennis": awayName = SimplifyTennisNames(awayName); break;
                case "Basketball": awayName = SimplifyBasketballName(awayName); break;
                case "Baseball": awayName = SimplifyBaseballName(awayName); break;
                case "Football": awayName = SimplifyNFLName(awayName); break;
            }
            //console.log("awayName: " + awayName);

            let switchedMatch = 9;

            if (matchNameEdited.split(" v ")[0].includes(homeName)) {
                if (matchNameEdited.split(" v ")[1].includes(awayName)) {
                    switchedMatch = 0;
                }
            }

            if (matchNameEdited.split(" v ")[0].includes(awayName)) {
                if (matchNameEdited.split(" v ")[1].includes(homeName)) {
                    switchedMatch = 1;
                }
            }
            if (switchedMatch === 9) {
                console.log(
                    "Don't know if match switched or not inside Snow... " +
                    matchNameEdited +
                    " home market: " +
                    homeName +
                    " away market: " +
                    awayName
                );
                continue;
            }

            let isSwitched = switchedMatch === 1 ? "True" : "False";
            console.log(
                "(Snow parsing) Snow match name: " +
                matchNameEdited +
                " home side: " +
                homeName +
                " away side: " +
                awayName +
                " swithced: " +
                isSwitched
            );

            fillSnowEvents(inputJson,snowEvent,switchedMatch,markets,lines,0); // 0 = home
            fillSnowEvents(inputJson,snowEvent,switchedMatch,markets,lines,1); // 1 = away
        }
        snowEvent.leagueName = league2;

        //console.log("Event: " + JSON.stringify(snowEvent, null, 2));
        _snowEventsArray.push(snowEvent);
    }
    //console.log("-------------------------------");
}

setInterval(LogMatchArray, 3000);

function LogMatchArray() {
    console.log("--------------------");
    console.log(JSON.stringify(_snowEventsArray, null, 3));
    console.log("--------------------");
}
