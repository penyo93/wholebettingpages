import test from 'node:test';
import * as assert from "assert";
import {SimplifyBaseballName,SimplifySoccerName} from "../simplifyNames.js";

test("should get soccer team name",(t) =>{
    assert.strictEqual(SimplifySoccerName("Nigeria - LIVE"),"Nigeria")
})


test("should get baseball name",(t)=> {
    assert.strictEqual("Astros - LIVE","Astros");
})