import puppeteer from 'puppeteer'
import { process} from "./process.js"

(async () => {
    await puppeteer
        .launch({
            headless: false,
        })
        .then(async (browser) => {
            const page = await browser.newPage();

            page.on("response", async (response) => {
                if (response.url().startsWith("https://api.tippmix.hu/tippmix/live-bet-page")) {
                    await response
                        .json()
                        .then(value => {
                            process(value.data.live)
                        })
                        .catch(err => console.log(err))
                }
            });

            await page.goto("https://www.tippmix.hu/elo", {
                waitUntil: "load",
                timeout: 0,
            });
        });
})()
