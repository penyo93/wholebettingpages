let matches = [];

const process = (liveData) => {
    liveData.forEach(value => {
        if (value.sportName === "Kosárlabda" && value.isLive === 1) {
            fetch("https://api.tippmix.hu/tippmix/live-event", {
                "headers": {
                    "accept": "application/json, text/plain, */*",
                    "accept-language": "hu-HU,hu;q=0.9,en-US;q=0.8,en;q=0.7",
                    "content-type": "application/x-www-form-urlencoded",
                    "sec-ch-ua": "\"Chromium\";v=\"118\", \"Google Chrome\";v=\"118\", \"Not=A?Brand\";v=\"99\"",
                    "sec-ch-ua-mobile": "?0",
                    "sec-ch-ua-platform": "\"Windows\"",
                    "sec-fetch-dest": "empty",
                    "sec-fetch-mode": "cors",
                    "sec-fetch-site": "same-site",
                    "Referer": "https://www.tippmix.hu/",
                    "Referrer-Policy": "strict-origin-when-cross-origin"
                },
                "body": '{"eventId":' + value.eventId + '}',
                "method": "POST"
            }).then(res => {
                res.json().then(res => {
                    let it = res.data[0];
                    console.log(it)
                    let structuredJson = {
                        id: it.eventId,
                        sport: "BASKETBALL",
                        name: it.eventName,
                        competitionName: it.competitionName,
                        competitionGroupName: it.competitionGroupName,
                        score: it.currentScore,
                        matchStatus: it.matchStatus + "_time_" + it.matchPlayTime,
                        odds: {}
                    }
                    it.markets.forEach(market => {
                        console.log(market)
                        let name = nameFormatter(market.marketName, market.outcomes[0].outcomeName, market.specialOddsValue, 0);
                        if (name !== "") {
                            structuredJson.odds[name] = market.outcomes[0].fixedOdds;
                            structuredJson.odds[name + "Id"] = market.marketRealNo;
                        }
                        name = nameFormatter(market.marketName, market.outcomes[1].outcomeName, market.specialOddsValue, 1);
                        if (name !== "") {
                            structuredJson.odds[name] = market.outcomes[1].fixedOdds;
                            structuredJson.odds[name + "Id"] = market.marketRealNo;
                        }

                    })
                    if (matches.length !== 0) {
                        matches = matches.filter(el => el.id !== it.eventId)
                    }
                    matches.push(structuredJson);
                }).catch(err => console.error(err))
            }).catch(err => console.error(err));
        }
    })

}


const getMatches = () => {
    return matches;
}

const nameFormatter = (marketName, teamNames, specialOddsValue, outComeIndex) => {
    let team = outComeIndex === 0 ? "home" : "away"
    if (marketName.includes("győztese") && !marketName.includes("negyed") && !marketName.includes("félidő")) {
        return team;
    } else if (marketName.includes("Hendikep") && !marketName.includes("negyed") && !marketName.includes("félidő")) {
        let handiCapNumber = teamNames.split(' ')[teamNames.split(' ').length -1]
        return `${team}_${handiCapNumber.replace("+","")}`
    } else if (marketName.includes("Pontszám") && !marketName.includes("negyed") && !marketName.includes("félidő")) {
        let direction = teamNames.includes("Kevesebb") ? "under" : "over";
        return `${direction}_${specialOddsValue}`
    } else if (marketName.includes("győztese") && marketName.includes("negyed")) {
        let quarter = marketName.substring(0,1)
        return `quarter!${quarter}!${team}`;
    } else if (marketName.includes("Hendikep") && marketName.includes("negyed")) {
        let quarter = marketName.substring(0,1)
        let handiCapNumber = teamNames.split(' ')[teamNames.split(' ').length -1]
        return `quarter!${quarter}!${team}_${handiCapNumber.replace("+","")}`
    } else if (marketName.includes("Pontszám") && marketName.includes("negyed")) {
        let direction = teamNames.includes("Kevesebb") ? "under" : "over";
        let quarter = marketName.substring(0,1)
        return `quarter!${quarter}!${direction}_${marketName.split(" ")[marketName.split(" ").length-1]}`
    } else if (marketName.includes("Hendikep") && marketName.includes("félidő")) {
        let handiCapNumber = teamNames.split(' ')[teamNames.split(' ').length -1]
        return `1stHalf!${team}_${handiCapNumber.replace("+","")}`
    } else if (marketName.includes("Pontszám") && marketName.includes("félidő")) {
        let direction = teamNames.includes("Kevesebb") ? "under" : "over";
        return `1stHalf!${direction}_${marketName.split(" ")[marketName.split(" ").length-1]}`
    } else {
        return "";
    }
}


export {process}