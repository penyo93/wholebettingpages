
const placeBet = (eventId, marketId, outcomeId, betSize, marketType, marketOdds) => {

    fetch("https://api1.sptpub.com/api/v2/coupon/brand/2103509236163162112/bet/place", {
        headers: {
            "accept": "application/json, text/plain, */*",
            "content-type": "application/json",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "cookie": "",//"activeLocale=en; _ga_BPN3NFS1PM=GS1.1.1673178678.1.0.1673178678.0.0.0; language=en-US; timezone=1; _device_type=desktop; search=; oddschangepolicy=0; _gid=GA1.2.1686155300.1675190935; ASP.NET_SessionId=sluefgjiuzeo4yyp2fmts4bp; SERVERID=N4; _ga=GA1.1.641729121.1673178593; _ga_7L9HDGXCG3=GS1.1.1675190935.8.1.1675191313.0.0.0; theme_mode=dark; amplitude_id_050a0e7dd157b31ed5348f70a572585enitrobetting.eu=eyJkZXZpY2VJZCI6ImQ4YmU3NjZhLWJmMzQtNDk2MC04MmU0LTIwNTBkNDUxMjZhOVIiLCJ1c2VySWQiOm51bGwsIm9wdE91dCI6ZmFsc2UsInNlc3Npb25JZCI6MTY3NTE5MTMyMDg5MiwibGFzdEV2ZW50VGltZSI6MTY3NTE5MTQ5OTE3OSwiZXZlbnRJZCI6MTQsImlkZW50aWZ5SWQiOjAsInNlcXVlbmNlTnVtYmVyIjoxNH0=; nano_player=eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJsb2dnZWRJblwiOnRydWUsXCJlcnJvck1lc3NhZ2VcIjpudWxsLFwidXNlcklkXCI6MjIyMjI1LFwidXNlcm5hbWVcIjpudWxsLFwiYmxvY2tlZFN0YXR1c1wiOlwiTk9ORVwiLFwiYXR0cmlidXRlc1wiOnt9LFwicmVzcG9uc2VTdGF0dXNcIjpcIk5PTkVcIixcInVzZXJTZXR0aW5nc1wiOnt9LFwiZW1haWxcIjpcImdhYm9yX2hvbnRpQHlhaG9vLmNvbVwiLFwicGhvbmVOdW1iZXJcIjpudWxsLFwiY291bnRyeVwiOlwiVVNcIixcImt5Y1N0YXR1c1wiOm51bGwsXCJ0b2tlblwiOm51bGwsXCJzZWNyZXRcIjpudWxsLFwidG9rZW5SZWZJZFwiOm51bGwsXCJjdXJyZW5jaWVzXCI6W10sXCJtZmFTdGF0dXNcIjpudWxsLFwibWZhUHJvdmlkZXJcIjpudWxsLFwiY2FzaW5vRW5hYmxlZFwiOnRydWUsXCJwb2tlckVuYWJsZWRcIjp0cnVlLFwicmFjaW5nRW5hYmxlZFwiOnRydWUsXCJzcG9ydHNib29rRW5hYmxlZFwiOnRydWUsXCJ0aWNrZXRzRW5hYmxlZFwiOnRydWUsXCJjb250ZXN0RW5hYmxlZFwiOnRydWUsXCJhdmF0YXJVcmxcIjpudWxsLFwiZW5hYmxlZFByb2R1Y3RzXCI6bnVsbCxcImJsb2NrZWRcIjpmYWxzZSxcImVtYWlsVmVyaWZpZWRcIjpmYWxzZSxcIm5ld1VzZXJcIjpmYWxzZSxcImt5Y1ZlcmlmaWVkXCI6ZmFsc2V9IiwiZXhwIjoxNjc1MjEzMTAxLCJpYXQiOjE2NzUxOTE1MDF9.sPG41xdTpMnoPaMiUL6ks_KxUjcZjwhusB94LAWBWV6d4o_L4DPhH2bIhArBRuhbLwLJHCEs6FHp3g0Y9Lmmog; .ASPXAUTH=FDF420F01F7B8D9C54846C6E7409E2EFF96BBD17D2F03F92E302D54B210B07BF7915A80718A6268C35840637FEE85E5047681FC47FDCE812EF68BEFA2BA36A0708E09BF91C4E4D55058B4DB28B03D8F5D75F7951F361EBEA5568E2F70256EA83DE7B32245D54F6A97C26B147C7DCBA4C6C3EE6D5F1BE799B615C0AD793863D4A; oddformat=decimal; viewformat=european",
            "Referer": "", //https://api1.sptpub.com/api/v2/coupon/brand/2103509236163162112/bet/place",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"
        },
        body: `{\\"type\\":\\"1/1\\",\\"sum\\":${betSize},\\"k\\",\\":${marketOdds}\\",\\"global_id\\":null\\,\\"bonus_id\\":null\\,\\"bet_request_id\\":"${eventId}-${marketId}-${marketType}-${outcomeId}\\",\\"selections\\":\\[{\\"event_id\\":\\"${eventId}"\\,"\\market_id":\\"${marketId}","specifier":"${marketType}","outcome_id":"${outcomeId}","k":"${marketOdds}","source":{"layout":"sidebar","page":"/:sportId/:categoryId/:tournamentId/:eventId","section":"","extra":{"market":"ALL","timeFilter":"","banner_type":""}},"promo_id":null,"bonus_id":null,"timestamp":${Date.now()}]}]`,
        method: "POST"
    })
        .then(response => response.json())
        .then(data => console.log(data))
        .catch(error => console.log(error));

}
//"bet_request_id":"2254064753515302919-225-total=221-12"

//[{"type":"1/1","sum":"1","k":"1.7","global_id":null,"bonus_id":null,"bet_request_id":"2254064753515302919-225-total=221-12","selections":[{"event_id":"2254064753515302919","market_id":"225","specifiers":"total=221","outcome_id":"12","k":"1.7","source":{"layout":"sidebar","page":"/:sportId/:categoryId/:tournamentId/:eventId","section":"","extra":{"market":"ALL","timeFilter":"","banner_type":""}},"promo_id":null,"bonus_id":null,"timestamp":1680442355685}]}]


//placeBet("2254064753515302919", "225", "12","3","total=221","1.7");
