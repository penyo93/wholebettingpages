import puppeteer from "puppeteer";
import {matches, proccess} from "./proccess.js";


let _versionNumber = 0;

let _sport = "FOOT";

(async () => {
    await puppeteer
        .launch({
            args: [
                '--disable-web-security',
                '--disable-features=IsolateOrigins',
                '--disable-site-isolation-trials'
            ],
            headless: false,
        })
        .then(async (browser) => {
            const page = await browser.newPage();
            page.on("response", async (res) => {
                if (res.url().startsWith("https://en.stoiximan.gr/danae-webapi/api/live/overview/")) {
                    await res
                        .json()
                        .then(value => {
                         _versionNumber = value.version;
                        })
                        .catch(err => console.error(err))
                }
                if (_versionNumber !== 0) {
                    fetch(`https://www.stoiximan.gr/danae-webapi/api/live/overview/${_versionNumber}?isInit=false`, {
                        "headers": {
                            "accept": "application/json, text/plain, */*",
                            "accept-language": "hu-HU,hu;q=0.9,en-US;q=0.8,en;q=0.7",
                            "sec-ch-ua": "\"Google Chrome\";v=\"117\", \"Not;A=Brand\";v=\"8\", \"Chromium\";v=\"117\"",
                            "sec-ch-ua-mobile": "?0",
                            "sec-ch-ua-platform": "\"Windows\"",
                            "sec-fetch-dest": "empty",
                            "sec-fetch-mode": "cors",
                            "sec-fetch-site": "same-origin",
                            "x-language": "2",
                            "x-operator": "2",
                            "cookie": "sb_landing=true; DV360=fired; OptanonAlertBoxClosed=2023-09-02T12:54:03.635Z; fs_uid=#o-1FC4W0-na1#4994912b-791b-4384-826e-9a450566dd17:7e2272c2-c4c2-4719-8650-9e29985a6c2d:1693820793212::1#/1725356792; cntps_id=28f5e8243829accdfadc801455d4709d; Add to Bet slip Time Stamp=1697187036504; __RequestVerificationToken=kRayI8v1k-ES2MLeo0Wtlo-43B615_FlN2YiVPwWhlKBjKlbMUbovagr65a7i9cpUj0k-A6ReRozh-Y_tZ4CTSg0lx6VUTZovWyo5trKBwE1; _tz=120; _tz_intl=Europe%2FBudapest; Real Referral=; __cf_bm=rY058qXR7FZwjVp3v919A_hJMl7KMVfDnaV8vPF9MSE-1697280517-0-AUz/6s7t8VoVXzKFcyRB582lFAQOZDYDve+4GJCAdhfrm/GefNBxleUvZQA4W0v+08+wXsRyDju343gH/eRFwak=; _cfuvid=1.8LAa_qNirKkXJiibS_B_YIJhc9vgx.cWvqxgjtrDA-1697280517667-0-604800000; OptanonConsent=isGpcEnabled=0&datestamp=Sat+Oct+14+2023+12%3A48%3A49+GMT%2B0200+(k%C3%B6z%C3%A9p-eur%C3%B3pai+ny%C3%A1ri+id%C5%91)&version=6.37.0&isIABGlobal=false&hosts=&consentId=3ce71b1a-8e62-4196-9908-b8b9ec3d30b3&interactionCount=1&landingPath=NotLandingPage&groups=C0001%3A1%2CC0002%3A0%2CC0003%3A0%2CC0004%3A0&geolocation=GR%3B&AwaitingReconsent=false; _lr_tabs_-7hhr6m%2Fstoiximangr={%22sessionID%22:0%2C%22recordingID%22:%225-d8f88433-9d2a-463a-a58f-b6e43179d24a%22%2C%22webViewID%22:null%2C%22lastActivity%22:1697280530405}; _lr_hb_-7hhr6m%2Fstoiximangr={%22heartbeat%22:1697280530406}; _fs_sample=false; _lr_uf_-7hhr6m=f88fe58e-1275-4c10-9ee5-9fe85d0f2b93",
                            "Referer": "https://www.stoiximan.gr/",
                            "Referrer-Policy": "strict-origin-when-cross-origin"
                        },
                        "body": null,
                        "method": "GET"
                    }).then(val => {
                        val.json().then(value => {
                            matches.length = 0
                            Object.values(value.events).forEach((value) => {
                                if (value.sportId === _sport) {
                                    proccess(value.url)
                                }
                            })
                        })
                    });
                }
            });

            await page.goto("https://en.stoiximan.gr/", {
                waitUntil: "load",
                timeout: 0,
            });

        });
})()
