import request from "request";

let matches = [];

const proccess = (eventUrl) => {
    console.log(eventUrl)
    request(getRequestOptions(eventUrl), (matchError, matchResponse, matchBody) => {
        let json = JSON.parse(matchBody)
        if (json['data']['event']['liveNow']) {
            oddsParser(json['data']['event'])
        }
    })
}


const oddsParser = (json) => {

    if (matches.length !== 0) {
        matches = matches.filter(it => it.name !== json.shortName)
    }

    let home = json.shortName.split("-")[0].trim();
    let away = json.shortName.split("-")[1].trim();
    let structuredJson = {
        id: json.id,
        sport: json.sportId === "BASK" ? "BASKETBALL" : "FOOTBALL",
        name: json.shortName,
        league: json.leagueDescription,
        matchStatus: matchStatusConverter(json.sportId, json.liveData),
        odds: {}
    }

    json.markets.forEach(it => {
        it.selections.forEach(market => {
            let name = "";
            if (json.sportId === "FOOT") {
                if (market.name.includes("Over")) {
                    name = footBallNameFormatter(it.name, market.name)
                }
            } else {
                name = basketballNameFormatter(it.name, home, away, market.handicap, market.name)
            }
            if (name !== "") {
                structuredJson.odds[name] = market.price
                structuredJson.odds[name + "Id"] = market.id
            }
        })
    })
    matches.push(structuredJson);
}

const matchStatusConverter = (sport, liveData) => {
    if (sport === "FOOT") {
        return `${liveData.clock.secondsSinceStart < 3600 ? `half_1_time_${liveData.clock.secondsSinceStart}` : `half_2_time_${liveData.clock.secondsSinceStart - 3600}`}`;
    } else {
        return "quarter_" + liveData.periodDescription.substring(1, 2) + "_time_" + liveData.clock.secondsSinceStart
    }
}


const footBallNameFormatter = (matchName, marketName) => {
    if (matchName === "Over/Under Total Goals") {
        return `${marketName.replace(' ', '_').toLowerCase()}`
    } else if (matchName === "Over/Under First Half Goals") {
        return `1stHalf!${marketName.replace(' ', '_').toLowerCase()}`
    } else if (matchName === "Asian (Over/Under) Goals" && marketName.split(" ")[1].split(".")[1] === "5") {
        return `${marketName.replace(' ', '_').toLowerCase()}`
    } else if (matchName === "Asian (Over/Under) - First Half Total Goals" && marketName.split(" ")[1].split(".")[1] === "5") {
        return `1stHalf!${marketName.replace(' ', '_').toLowerCase()}`
    } else {
        return ""
    }
}


const getMatches = () => {
    return matches;
}

const basketballNameFormatter = (name, home, away, handicap, marketName) => {
    if (name.includes("Winner") && marketName.includes(home) && !name.includes("Period") && !name.includes("Half")) {
        return `home`
    } else if (name.includes("Winner") && marketName.includes(away) && !name.includes("Period") && !name.includes("Half")) {
        return `away`
    } else if (name.includes("Handicap") && marketName.includes(home) && !name.includes("Period") && !name.includes("Half")) {
        return `home_${handicap}`
    } else if (name.includes("Handicap") && marketName.includes(away) && !name.includes("Period") && !name.includes("Half")) {
        return `away_${handicap}`
    } else if (name.includes("Total") && !name.includes("Odd") && !name.includes("Period") && !name.includes("Half")) {
        return `${marketName.replace(" ", "_").toLowerCase()}`
    } else if (name.includes("Winner") && marketName.includes(home) && name.includes("Period") && !name.includes("Half")) {
        return name.includes("Second") ? `quarter!2!home` : name.includes("Third") ? `quarter!3!home`
            : name.includes("First") ? `quarter!1!home` : `quarter!${name.substring(0, 1)}!home`
    } else if (name.includes("Winner") && marketName.includes(away) && name.includes("Period") && !name.includes("Half")) {
        return name.includes("Second") ? `quarter!2!away` : name.includes("Third") ? `quarter!3!away` :
            name.includes("First") ? `quarter!1!away` : `quarter!${name.substring(0, 1)}!away`
    } else if (name.includes("Handicap") && marketName.includes(home) && name.includes("Period") && !name.includes("Half")) {
        return name.includes("Second") ? `quarter!2!home_${handicap}` : name.includes("Third") ? `quarter!3!home_${handicap}` :
            name.includes("First") ? `quarter!1|home_${handicap}` : `quarter!${name.substring(0, 1)}!home_${handicap}`
    } else if (name.includes("Handicap") && marketName.includes(away) && name.includes("Period") && !name.includes("Half")) {
        return name.includes("Second") ? `quarter!2!away_${handicap}` : name.includes("Third") ? `quarter!3!away_${handicap}` :
            name.includes("First") ? `quarter|1|away_${handicap}` : `quarter!${name.substring(0, 1)}!away_${handicap}`
    } else if (name.includes("Total") && !name.includes("Odd") && name.includes("Period") && !name.includes("Half")) {
        return name.includes("Second") ? `quarter!2!${marketName.replace(" ", "_").toLowerCase()}` : name.includes("Third") ?
            `quarter!3!${marketName.replace(" ", "_").toLowerCase()}` : name.includes("First") ? `quarter!1!${marketName.replace(" ", "_").toLowerCase()}`
                : `quarter!${name.substring(0, 1)}!${marketName.replace(" ", "_").toLowerCase()}`
    } else if (name.includes("Winner") && marketName.includes(home) && !name.includes("Period") && name.includes("Half")) {
        return name.includes("Second") ? `quarter!4!home` : `1stHalf!home`
    } else if (name.includes("Winner") && marketName.includes(away) && !name.includes("Period") && name.includes("Half")) {
        return name.includes("Second") ? `quarter!4!home` : `1stHalf!away`
    } else if (name.includes("Handicap") && marketName.includes(home) && !name.includes("Period") && name.includes("Half")) {
        return name.includes("Second") ? `quarter!4!home` : `1stHalf!home_${handicap}`
    } else if (name.includes("Handicap") && marketName.includes(away) && !name.includes("Period") && name.includes("Half")) {
        return name.includes("Second") ? `quarter!4!home` : `1stHalf!away_${handicap}`
    } else if (name.includes("Total") && !name.includes("Odd") && !name.includes("Period") && name.includes("Half")) {
        return name.includes("Second") ? `quarter!4!home` : `1stHalf!${marketName.replace(" ", "_").toLowerCase()}`
    }
    return ""
}

const getRequestOptions = (eventUrl) => {
    return {
        "url": `https://en.stoiximan.gr/api${eventUrl}`,
        "headers": {
            "accept": "application/json, text/plain, */*",
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
            "Referer": "https://en.stoiximan.gr/",
            "Referrer-Policy": "strict-origin-when-cross-origin",
        },
        "body": null,
        "method": "GET"
    };
}


export {proccess, matches}