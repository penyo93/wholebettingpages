import puppeteer from "puppeteer";


let _cookie = [];

let _userAgent = "";

(async () => {
    await puppeteer
        .launch({
            args: [
                '--disable-web-security',
                '--disable-features=IsolateOrigins',
                '--disable-site-isolation-trials'
            ],
            headless: false,
        })
        .then(async (browser) => {
            const page = await browser.newPage();
            page.on("request", (req) => {
              //  console.log("**" + req.url())
                page.cookies().then(it => {
                    if (it.length !== 0) {
                        it.forEach(it => {
                            if (!_cookie.some(el => el.split('=')[0] === it.name)) {
                                _cookie.push(`${it.name}=${it.value}`)
                            } else {
                                let index = _cookie.findIndex(el => el.split(':')[0] === it.name)
                                _cookie[index] = `${it.name}=${it.value}`;
                            }

                        })
                    }
                })
                const userAgent = req.headers()["user-agent"]
                if (userAgent !== undefined) {
                    if (userAgent !== _userAgent) {
                        console.log(userAgent);
                        _userAgent = userAgent;
                    }
                }
            });


            await page.goto("https://en.stoiximan.gr/", {
                waitUntil: "load",
                timeout: 0,
            });

        });
})()


export {_cookie, _userAgent}