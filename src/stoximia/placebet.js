import {_cookie, _userAgent} from "./placeBetStart.js";

let _hash = "";
let _betSlipTrackId = "";
let _legs = [];
let _oddsAmount = 0;

const placeBet = (betSize, tag) => {
    const interval = setInterval(async () => {
        if (_cookie.filter(it => it.split("=")[0] === "pocaauth").length === 1) {
            _cookie.push(`Add to Bet slip Time Stamp=${Date.now()}`)
            await getPlaceBetDatas(tag)
            if (_hash !== "" && _legs.length !== 0 && _oddsAmount !== 0 && _betSlipTrackId !== "") {
                _cookie.push(`Add to Bet slip Time Stamp=${Date.now()}`)
                let betSlipIndex = _cookie.findIndex(x => x.split("=")[0].includes("Bet"))
                _cookie[betSlipIndex] = `Add to Bet slip Time Stamp=${Date.now()}`;
                _cookie.push(`Place bet Time Stamp=${Date.now()}`)
                _cookie.push("DV360=fired")
                _cookie.push("isDeposited=Depositor")
                _cookie.push("fs_uid=#o-1FC4W0-na1#4994912b-791b-4384-826e-9a450566dd17:7e2272c2-c4c2-4719-8650-9e29985a6c2d:1693820793212::1#/1725356792")
                await updateBets(tag, betSize, interval);
                clearInterval(interval)
            }
        }
    }, 1000)
}

const getPlaceBetDatas = async (tag) => {
    fetch("https://www.stoiximan.gr/api/betslip/v3/plain-leg/", {
        "headers": {
            "accept": "application/json, text/plain, */*",
            "accept-language": "hu-HU,hu;q=0.9,en-US;q=0.8,en;q=0.7",
            "content-type": "application/json",
            "sec-ch-ua": "\"Chromium\";v=\"116\", \"Not)A;Brand\";v=\"24\", \"Google Chrome\";v=\"116\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"Windows\"",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "cookie": _cookie.join("; "),
            "Referer": "https://www.stoiximan.gr/",
            "Referrer-Policy": "strict-origin-when-cross-origin",
            "user-agent": _userAgent
        },
        "body": '{"selectionIds":["' + tag + '"],"betslip":{"hash":"","slipData":"","legs":[],"bets":[],"betslipTabId":1,"betslipTrackId":""}}',
        "method": "POST"
    }).then(res => {
        console.log("Lement a hívás")
        res.json().then(value => {
            _hash = value.data.hash;
            _betSlipTrackId = value.data.betslipTrackId;
            _legs = value.data.legs;
            _oddsAmount = value.data.legs[0].odds
        })

    }).catch(err => console.error(err));
}

const updateBets = async (tag, betSize, interval) => {
    fetch("https://en.stoiximan.gr/api/betslip/v3/updatebets", {
        "headers": {
            "accept": "application/json, text/plain, */*",
            "accept-language": "hu-HU,hu;q=0.9,en-US;q=0.8,en;q=0.7",
            "content-type": "application/json",
            "sec-ch-ua": "\"Google Chrome\";v=\"117\", \"Not;A=Brand\";v=\"8\", \"Chromium\";v=\"117\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"Windows\"",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "cookie": _cookie.join(" ;"),
            "Referer": "https://en.stoiximan.gr/",
            "Referrer-Policy": "strict-origin-when-cross-origin"
        },
        "body": '{"betslip":{"hash":"' + _hash + '","slipData":"' + _hash + '","legs":' + JSON.stringify(_legs) + ',"bets":[{"id":"1:SGL:' + tag + '","amount":0,"minAmount":0,"maxAmount":0,"odds":' + _oddsAmount + ',"tag":"' + tag + '","numLines":1,"type":"SGL","typeName":"Single","bonusTokenId":"","betslipTabId":1}],"betslipTabId":1,"betslipTrackId":"' + _betSlipTrackId + '"},"bets":[{"id":"1:SGL:' + tag + '","amount":' + betSize + ',"minAmount":0,"maxAmount":0,"odds":' + _oddsAmount + ',"tag":"' + tag + '","numLines":1,"type":"SGL","typeName":"Single","bonusTokenId":"","betslipTabId":1}]}',
        "method": "PATCH"
    }).then(res => {
        res.json().then(value => {
                _hash = value.data.hash;
                fetch("https://en.stoiximan.gr/api/betslip/v3/place", {
                    "headers": {
                        "accept": "application/json, text/plain, */*",
                        "accept-language": "hu-HU,hu;q=0.9,en-US;q=0.8,en;q=0.7",
                        "content-type": "application/json",
                        "sec-ch-ua": "\"Chromium\";v=\"116\", \"Not)A;Brand\";v=\"24\", \"Google Chrome\";v=\"116\"",
                        "sec-ch-ua-mobile": "?0",
                        "sec-ch-ua-platform": "\"Windows\"",
                        "sec-fetch-dest": "empty",
                        "sec-fetch-mode": "cors",
                        "sec-fetch-site": "same-origin",
                        "cookie": _cookie.join("; "),
                        "Referer": "https://en.stoiximan.gr/",
                        "Referrer-Policy": "strict-origin-when-cross-origin",
                        "user-agent": _userAgent
                    },
                    "body": '{"betslip":{"hash":"' + _hash + '","slipData":"' + _hash + '","legs":' + JSON.stringify(_legs) + ',"bets":[{"id":"1:SGL:' + tag + '","amount":' + betSize + ',"minAmount":0,"maxAmount":0,"odds":' + _oddsAmount + ',"tag":"' + tag + '","numLines":1,"type":"SGL","typeName":"Single","bonusTokenId":"","betslipTabId":1}],"betslipTabId":1,"betslipTrackId":"' + _betSlipTrackId + '","oddschanges":"1"}}',
                    "method": "POST"
                }).then(res => {
                    clearInterval(interval)
                    res.json().then(value =>
                        console.log(value)
                    )
                }).catch(err => console.error(err));
            }
        )
    }).catch(err => console.error(err));
}

placeBet(0.1, "3924168198");