import {_userAgent, _pamesAuth} from './placeBetStart.js'

const placeBet = (oddsID, betSize, numerator, denominator) => {
    const interval = setInterval(() => {
        if ( _userAgent !== "" && _pamesAuth !== "") {
            fetch(
                "https://papi.pamestoixima.gr/bet-service/api/v1/slips",
                {
                    "headers": {
                        "accept": "*/*",
                        "authorization": _pamesAuth,
                        "content-type": "application/json",
                        "sec-ch-ua": '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
                        "sec-ch-ua-mobile": "?0",
                        "sec-ch-ua-platform": '"Windows"',
                        "sec-fetch-dest": "empty",
                        "sec-fetch-mode": "cors",
                        "sec-fetch-site": "same-site",
                        "x-accept-language": "en-GB",
                        "user-agent": _userAgent,
                        "x-ob-channel": "I",
                        "Referer": "https://www.pamestoixima.gr/",
                        "Referrer-Policy": "strict-origin-when-cross-origin",
                    },
                    "body": '{"legs":[{"documentId":"LEG1","options":{"banker":false,"inMultiples":true,"handicaps":[{"outcomeRef":"'+oddsID+'"}],"prices":[{"priceTypeRef":"LP","numerator":'+numerator+',"denominator":'+denominator+',"winPlaceRef":"WIN"}],"winPlace":"WIN"},"outcomes":[{"outcomeRef":"'+oddsID+'"}]}],"bets":[{"documentId":"BET1","betTypeRef":"SGL","lines":1,"legRefs":["LEG1"],"options":{"winPlaceOverride":"WIN","priceChangeBehaviour":"NONE"},"stake":'+betSize+',"stakeType":"SPL"}]}',
                    "method": "POST"
                }).then(res => {
                console.log(res)
                clearInterval(interval)
            }).catch(err => console.log(err));
        }
    }, 500)
}

placeBet(58136799, 0.1,9,10)
