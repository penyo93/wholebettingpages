import puppeteer from 'puppeteer-extra'
import {tennisProcess} from "./tennisProcess.js";
import {basketBallProcess} from "./basketBallProcess.js"
import ws from "ws"
import  StealthPlugin from "puppeteer-extra-plugin-stealth"

let sportName = "TENNIS";

puppeteer.use(StealthPlugin());

(async () => {
    await puppeteer
        .launch({
            headless: false
        })
        .then(async (browser) => {
            const page = await browser.newPage();
            const client = await page._client();
        //     const cdp = await page.target().createCDPSession();
        //     await cdp.send("Network.enable");
        //     await cdp.send("Page.enable");
        //
        //    cdp.on("Network.webSocketFrameReceived",handleWebSocketFrameReceived)
        //     await cdp.on('Network.webSocketFrameReceived', (event) => {
        //         console.log('WebSocket üzenet érkezett:', event.response.payloadData);
        //     });

            client.on("Network.webSocketCreated", ({ requestId, url }) => {
                console.log("Network.webSocketCreated");

            });


            client.on("Network.webSocketFrameReceived", ({ requestId, timestamp, response }) => {
                const data = response.payloadData;
                console.log(data)
                // const messages = toMessages(data);
                // messages.forEach((message) => {
                //     console.log("received message:" + JSON.stringify(message));
                //
                // });
            });
            await page.goto("https://www.pamestoixima.gr/en",{
                waitUntil: "load",
                timeout: 0,
            });

        });
})()

const handleWebSocketFrameReceived = (params) => {
    const payload = params.response.payloadData;
    console.log(payload)
}

const getLiveEvents = async () => {
    await puppeteer
        .launch({
            args: [
                '--disable-web-security',
                '--disable-features=IsolateOrigins',
                '--disable-site-isolation-trials'
            ],
            headless: false,
        })
        .then(async (browser) => {
            const basketBallPage = await browser.newPage();
            const tennisPage = await browser.newPage();



            await basketBallPage.goto("https://capi.pamestoixima.gr/content-service/api/v1/q/eventsListBySport?drilldownTagIds=5&groupCodes=HANDICAP_2_WAY,TOTAL_POINTS_OVER/UNDER,MONEY_LINE&sortsIncluded=MTCH&liveNow=true&orderEventsBy=displayOrder&excludeEventsWithNoMarkets=false", {
                waitUntil: "load",
                timeout: 0,
            });
            await tennisPage.goto("https://capi.pamestoixima.gr/content-service/api/v1/q/eventsListBySport?drilldownTagIds=12&groupCodes=MATCH_WINNER,TOTAL_GAMES_OVER/UNDER,TOTAL_SETS_OVER/UNDER,GAME_HANDICAP&sortsIncluded=MTCH&liveNow=true&orderEventsBy=displayOrder&excludeEventsWithNoMarkets=fals", {
                waitUntil: "load",
                timeout: 0,
            });
            setInterval(async () => {
                if (sportName === "TENNIS") {
                    await tennisPage.reload();
                    await tennisPage.setDefaultNavigationTimeout(0);
                    await tennisPage.waitForNavigation();
                    const tennisExtractedText = await tennisPage.$eval('*', (el) => el.innerText);
                    tennisProcess(tennisExtractedText)
                } else {
                    await basketBallPage.reload();
                    await basketBallPage.setDefaultNavigationTimeout(0);
                    await basketBallPage.waitForNavigation();
                    const basketBallExtractedText = await basketBallPage.$eval("*", (el) => el.innerText);
                    basketBallProcess(basketBallExtractedText)
                }
            }, 5000)
        });
}

