import puppeteer from "puppeteer";

let _pamesAuth = "";
let _userAgent = "";

(async () => {
    await puppeteer
        .launch({
            args: [
                '--disable-web-security',
                '--disable-features=IsolateOrigins',
                '--disable-site-isolation-trials'
            ],
            headless: false,
        })
        .then(async (browser) => {
            const page = await browser.newPage();
            page.on("request", (req) => {
                if (req.url().includes(".gr")) {
                    const auth = req.headers()["authorization"];
                    const userAgent = req.headers()["user-agent"]
                    if (auth !== undefined) {
                        if (auth.startsWith("Bearer")) {
                            if (auth !== _pamesAuth) {
                                console.log(auth);
                                _pamesAuth = auth;
                            }
                        }
                    }
                    if (userAgent !== undefined) {
                        if (userAgent !== _userAgent) {
                            console.log(userAgent);
                            _userAgent = userAgent;
                        }
                    }
                }
            });

            await page.goto("https://www.pamestoixima.gr/en", {
                waitUntil: "load",
                timeout: 0,
            });

        });
})()


export {_pamesAuth, _userAgent}