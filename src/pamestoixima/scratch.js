async function StartNewTab(liveMatchIndex) {
    let page = await _browser.newPage();

    await page.goto(_liveMatchesArray[liveMatchIndex].split("!")[0]);
    let matchName = _liveMatchesArray[liveMatchIndex].split("!")[1];

    const cdp = await page.target().createCDPSession();
    await cdp.send("Network.enable");
    await cdp.send("Page.enable");

    function printReceiveResponse(response) {
        if (response.response.payloadData.includes("sportFixtureMarketsNext")) {
            ParseStakeOddsUpdate(response.response.payloadData, matchName);
        } else if (response.response.payloadData.includes("sportFixture")) {
            ParseStakeMatchInfoUpdate(response.response.payloadData, matchName);
        }
        return;
    }

    cdp.on("Network.webSocketFrameReceived", printReceiveResponse); // Fired when WebSocket message is received.
}