import request from 'request'
import {createJsonObjects} from './sportBetStructure.js'

//1166

const getMatch = (eventIds, sportName) => {
    if (eventIds.size > 0) {

        eventIds.forEach(id => {
            const options = GetRequestOptions(id);
            if(options === undefined) return
            request(options, (matchError, matchResponse, matchBody) => {
                if (matchBody === undefined) return
                if (JSON.parse(matchBody)[0] !== undefined) {
                    let status = JSON.parse(matchBody)[0]['Status']
                    let sportType = JSON.parse(matchBody)[0]['SportTypeID']
                    let eventId = JSON.parse(matchBody)[0]['EventID']
                    if (sportName === "BASKETBALL" ? sportType === 1172 || sportType === 2357 : sportType === 1166) {
                        request(`https://ultraplay.nitrobetting.eu/api/bettingfeed/preview/markets?LangCode=en-US&MatchIds=${id}`, (error, response, body) => {
                            createJsonObjects(eventId, status, JSON.parse(body)[0]['ActiveOdds'], JSON.parse(body)[0]['ActiveMarkets'])
                        })
                    }
                }
            })

        })

    }
}
function GetRequestOptions(id) {
    return {
        "url": `https://ultraplay.nitrobetting.eu/api/bettingfeed/matchinfo?&EventIds=${id}`,
        "headers": {
            "accept": "application/json, text/plain, */*",
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
            "Referer": "https://ultraplay.nitrobetting.eu",
            "Referrer-Policy": "strict-origin-when-cross-origin",
        },
        "body": null,
        "method": "GET"
    };
}

 export {getMatch};

