
const events = [];

const GetEvents = () => {
    return events;
};

const createJsonObjects = (
    eventId,
    status,
    activeOdds,
    activeMarkets,
) => {
    const event = {};
    event.odds = {};
    let activeBettingMarketIds = new Set(
        activeMarkets
            ?.filter((market) => market.BettingAvailability === 1)
            .map((market) => market.ID)
    );

    activeOdds?.forEach((odd) => {
        if (activeBettingMarketIds.has(odd.MarketID)) {
            if (odd.Value === 1) {
                return;
            }

            let homeTeam = activeOdds
                .filter((event) => event.TitleSuffix === null && event.Name === "1")
                .map((name) => name.Title);
            let awayTeam = activeOdds
                .filter((event) => event.TitleSuffix === null && event.Name === "2")
                .map((name) => name.Title);

            if (homeTeam.includes(",")) {
                homeTeam = homeTeam.split(",")[0];
            }

            if (awayTeam.includes(",")) {
                awayTeam = awayTeam.split(",")[0];
            }

            const matchName = homeTeam + " - " + awayTeam;
            const marketName = activeMarkets.find(
                (market) => market.ID === odd.MarketID
            ).Name;

            let prefix = "";
            if (marketName.includes("Half") || marketName.includes("half")) {
                prefix = "1stHalf!";
            } else if (
                marketName.includes("Quarter") ||
                marketName.includes("quarter")
            ) {
                return;
            }

            event["name"] = matchName;
            event["eventId"] = eventId;

            if (odd.Title === "Over" || odd.Title === "Under") {
                const key = "" + odd.Title.toLowerCase() + "_" + odd?.TitleSuffix;

                Object.assign(event.odds, {
                    [prefix + key + ""]: odd?.Value,
                    [prefix + key + "Id"]: odd?.ID,
                });
            } else if (odd?.TitleSuffix === null) {
                const title = odd?.Name === "1" ? "home" : "away";

                Object.assign(event.odds, {
                    [prefix + title + ""]: odd?.Value,
                    [prefix + title + "Id"]: odd?.ID,
                });

            } else {
                const title = odd.Name === "1" ? "home" : "away";

                let keyNumber = odd?.TitleSuffix;

                if (keyNumber.includes("+")) {
                    keyNumber = keyNumber.replace("+", "");
                }

                Object.assign(event.odds, {
                    [prefix + title + "_" + keyNumber]: odd?.Value,
                    [prefix + title + "_" + keyNumber + "Id"]: odd?.ID,
                });

            }
        }
    });

    const findIndex = events.findIndex((a) => a.eventId === event.eventId);

    if (findIndex !== -1) {
        events.splice(findIndex, 1);
    }

    events.push(event);
};



export { createJsonObjects, GetEvents};
