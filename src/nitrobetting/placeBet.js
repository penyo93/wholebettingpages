const placeBet = (marketID, oddSize, betSize) => {
    fetch("https://ultraplay.nitrobetting.eu/api/betslip/place", {
        "headers": {
            "accept": "application/json, text/plain, */*",
            "accept-language": "hu-HU,hu;q=0.9,en-US;q=0.8,en;q=0.7",
            "content-type": "application/json",
            "sec-ch-ua": "\"Not/A)Brand\";v=\"99\", \"Google Chrome\";v=\"115\", \"Chromium\";v=\"115\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"Windows\"",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "cookie": "activeLocale=en; _ga=GA1.2.2049541974.1680954688; _gid=GA1.2.17817388.1692123789; _ga_7L9HDGXCG3=GS1.1.1692123788.5.1.1692123800.0.0.0; theme_mode=dark; _fw_crm_v=eecfc093-82a1-46db-b757-6d920e9d5376; amplitude_id_050a0e7dd157b31ed5348f70a572585enitrobetting.eu=eyJkZXZpY2VJZCI6IjgyMWM3MjkxLTlhMTktNGMzYS1iZGQwLTY1MGY5NTZjZDJhMFIiLCJ1c2VySWQiOm51bGwsIm9wdE91dCI6ZmFsc2UsInNlc3Npb25JZCI6MTY5MjEyMzgwNDc0MCwibGFzdEV2ZW50VGltZSI6MTY5MjEyNDExMDQwNywiZXZlbnRJZCI6MiwiaWRlbnRpZnlJZCI6MCwic2VxdWVuY2VOdW1iZXIiOjJ9; nano_player=eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJsb2dnZWRJblwiOnRydWUsXCJlcnJvck1lc3NhZ2VcIjpudWxsLFwidXNlcklkXCI6MjIyMjI1LFwidXNlcm5hbWVcIjpudWxsLFwiYmxvY2tlZFN0YXR1c1wiOlwiTk9ORVwiLFwiYXR0cmlidXRlc1wiOnt9LFwicmVzcG9uc2VTdGF0dXNcIjpcIk5PTkVcIixcInVzZXJTZXR0aW5nc1wiOnt9LFwiZW1haWxcIjpcImdhYm9yX2hvbnRpQHlhaG9vLmNvbVwiLFwicGhvbmVOdW1iZXJcIjpudWxsLFwiY291bnRyeVwiOlwiVVNcIixcImt5Y1N0YXR1c1wiOm51bGwsXCJ0b2tlblwiOm51bGwsXCJzZWNyZXRcIjpudWxsLFwidG9rZW5SZWZJZFwiOm51bGwsXCJjdXJyZW5jaWVzXCI6W10sXCJtZmFTdGF0dXNcIjpudWxsLFwibWZhUHJvdmlkZXJcIjpudWxsLFwiY2FzaW5vRW5hYmxlZFwiOnRydWUsXCJwb2tlckVuYWJsZWRcIjp0cnVlLFwicmFjaW5nRW5hYmxlZFwiOnRydWUsXCJzcG9ydHNib29rRW5hYmxlZFwiOnRydWUsXCJ0aWNrZXRzRW5hYmxlZFwiOnRydWUsXCJjb250ZXN0RW5hYmxlZFwiOnRydWUsXCJhdmF0YXJVcmxcIjpudWxsLFwiZW5hYmxlZFByb2R1Y3RzXCI6bnVsbCxcImJsb2NrZWRcIjpmYWxzZSxcImVtYWlsVmVyaWZpZWRcIjpmYWxzZSxcIm5ld1VzZXJcIjpmYWxzZSxcImt5Y1ZlcmlmaWVkXCI6ZmFsc2V9IiwiZXhwIjoxNjkyMTQ1Njk5LCJpYXQiOjE2OTIxMjQwOTl9.L-UHa6NyZ_hQqmz7IU-Ph3pkzUL1ke9-yi7NuqoN03JEaRuwtJd2iNcaWtGI0jUOFTRTj18H79iMEGdTShsPPg; ASP.NET_SessionId=vehettugzvnnie3qgzngggcl; _device_type=desktop; language=en-US; search=; .ASPXAUTH=6C97969BED4438BB363BCA937C5EA82D50207DA9762704DE315B8BA268F0BB7DC1207DCCCE050610FD681DEDAF6B133268EC5897C48DF1D69D6A35A282841A79EA91231D049A97D235EC30178206249481AE036CB1C0DF31D35DE4768FFD53E04875F6E7093A0D294BE94644BF8640CA8C55F326460056C41BCA2A7CACC625DC; timezone=1; oddformat=decimal; viewformat=european; oddschangepolicy=0; SERVERID=N1",
            "Referer": "https://ultraplay.nitrobetting.eu/sport",
            "Referrer-Policy": "strict-origin-when-cross-origin",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"
        },
        "body": `{\"BetType\":\"single\",\"AcceptBetterOdds\":0,\"IsInsured\":false,\"Selections\":[{\"Items\":[{\"ID\":${marketID},\"OddValue\":${oddSize},\"IsBanker\":false,\"PitcherOption\":0}],\"Stake\":\"${betSize}\"}]}`,
        "method": "POST"
    })
        .then(response => response.json())
        .then(data => console.log(data))
        .catch(error => console.log(error));
 }

placeBet("331694586", 1.83, 0.1);


