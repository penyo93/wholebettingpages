import puppeteer from 'puppeteer'
import {getMatch} from './proccess.js'


let sportName = "BASKETBALL";

(async () => {
    await puppeteer
        .launch({
            headless: false,
        })
        .then(async (browser) => {
            const page = await browser.newPage();

            page.on("response", async (response) => {
                if (response.url().startsWith("https://ultraplay.nitrobetting.eu/signalr/")) {
                    await response
                        .json()
                        .then(value => {
                            if (value['M'] !== undefined && value['M'].length > 0) {
                                let events = [];
                                let eventsIds = new Set();
                                events.push(value["M"])
                                events
                                    .flatMap(x => x)
                                    .flatMap(x => x['A'])
                                    .flatMap(x => x)
                                    .filter(x => Number.isInteger(x))
                                    .forEach(x => eventsIds.add(x));
                                getMatch(eventsIds, sportName)
                                events.length = 0;
                                eventsIds.clear()
                            }
                        })
                        .catch(err => console.log(err))
                }
            });

            await page.goto("https://nitrobetting.eu/sportsbook/", {
                waitUntil: "load",
                timeout: 0,
            });
        });
})()
